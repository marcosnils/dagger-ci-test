import sys
from urllib.parse import urljoin
import anyio
import dagger
import os


async def test():
    async with dagger.Connection(dagger.Config(log_output=sys.stderr)) as client:

        # ======================== POSTGRES ========================
        database = (
            # use a postgres container
            client.container().from_("postgres:12")
            .with_env_variable("POSTGRES_PASSWORD", "")
            .with_env_variable("POSTGRES_HOST_AUTH_METHOD", "trust")
            .with_env_variable("POSTGRES_DB", "test")
            .with_exec(["postgres"])
            .with_exposed_port(5432)
        )

        # ======================== PYTHON ========================
        python = (
            # use a python 3.10 container
            client.container().from_("python:3.10-slim-buster")
            # bind postgres service
            .with_service_binding("db", database.as_service())
            .with_env_variable("DB_HOST", "db")
            .with_env_variable("DB_PASSWORD", "")
            .with_env_variable("DB_USER", "test")
            .with_env_variable("DB_NAME", "test")
            .with_exec(["apt", "update"])
            .with_exec(["apt", "install", "-y", "postgresql-client"])
            .with_exec(["pg_isready", "-d", "test", "-h", "db", "-U", "postgres"])

        )

        # execute
        await python.sync()

if __name__ == "__main__":
    anyio.run(test)
